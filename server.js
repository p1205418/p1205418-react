var express = require('express');
var app = module.exports = express();
const enableWs = require('express-ws')(app) ;
var path = require('path');
app.use(express.static(path.resolve('node_modules')));
app.use(express.static(path.resolve('client/css'))) ;
app.use(express.static(path.resolve('client/javascript'))) ;
app.use(express.static(path.resolve('client/resources'))) ;
app.use(express.static(path.resolve('dist'))) ;

let todos = {} ;

app.get('/', function(req, res){
    res.sendFile(__dirname + '/client/html/index.html');
});



app.ws('/websocket', function(ws, req, next){
    ws.on('message', function(msg) {
        var todo = JSON.parse(msg);
        todos[todo.id] = todo ;
        clientwebsocket.clients.forEach(function (client) {
            client.send(msg);
        });
    });
    ws.send(JSON.stringify({ todos : todos }) );
});
var clientwebsocket = enableWs.getWss('/websocket');


app.listen(3000);