/**
 * Created by Nathan on 04/04/2017.
 */
module.exports = function(grunt) {
    grunt.initConfig({

        express: {
            build: {
                options : {
                    server: ('server.js'),
                    hostname:"localhost"
                }
            }
        },
        jshint: {
            files: ['*.js'],
            options: {
                globals: {
                    jQuery: true
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-express');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.registerTask('build', ['express', 'express-keepalive']);

};