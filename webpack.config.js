//var HtmlWebpackPlugin = require('html-webpack-plugin');
module.exports = {
    /*plugins: [
        new HtmlWebpackPlugin({
            title: 'Todo app',
            filename: 'dist/index.html',
            template : "client/html/index.html"
        })
    ],*/
    entry: './client/javascript/todos.jsx',
    output: {
        path: __dirname,
        filename: 'dist/bundle.js'
    },
    node: {
        fs :"empty"
    },
    module: {
        loaders: [
            {
                test: /.jsx?$/,
                loader: 'babel-loader',
                exclude: /node_modules/,
                query: {
                    presets: ['es2015', 'react']
                }
            },
            { test: /\.(woff2?|ttf|eot|svg)$/, loader: 'url-loader' },
            { test: /\.css$/, loaders: ['style-loader', 'css-loader'] },
        ]
    },
};