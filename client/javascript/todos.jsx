window.jQuery=window.$ = require("jquery");
let React = require('react');
let ReactDOM = require('react-dom');
let Backbone = require('backbone');
let backboneMixin = require('backbone-react-component');
let collections = require('./collections.js');
require('bootstrap/dist/css/bootstrap.css');
require('bootstrap');
let todo_items = collections.todo_items ;
let socket = collections.ws ;


socket.onmessage=function(event) {
    var data = JSON.parse(event.data);
    var id = data.id ;
    console.log(data);
    if(data.todos !=  undefined){ // Cas où il recoit tous les précédents messages
        for(var i in data.todos){
            todo_items.add(data.todos[i]) ;
        }
    }else if(todo_items.get(id) !=  undefined){ // Mise à jour de l'état
        console.log(todo_items.get(id));
        todo_items.get(id).set(data);
    }else{                                  //Ajout d'un item
        todo_items.add(new collections.Todo(data));
    }
};

socket.onerror = function(err) {
    console.log(err);
};

socket.onclose = function (event) {
    console.log("Fin de communication");
};

socket.onopen = function(event) {
    var TodoBanner = React.createClass({
        render: function () {
            return (
                <h3 className="col-md-offset-4 col-md-4">La liste de tâches : {this.props.listetodos.length} tâches</h3>
            );
        }
    });
    var TodoListItem = React.createClass({
        mixins: [backboneMixin],
        handleClickItem: function (event) {
            this.props.todo.set("done", !this.props.todo.get("done"));
            socket.send(JSON.stringify(this.props.todo.attributes));
            event.preventDefault();
        },
        render: function () {
            var displayperson = this.props.todo.get("person") == "" ? "personne" : this.props.todo.get("person");
            return (
                <li className="list-group-item">
                    <div className="col-md-offset-2 checkbox">
                        <input type="checkbox" onChange={this.handleClickItem}
                               checked={this.props.todo.attributes.done}/>{this.props.todo.get("txt")} attribué
                        à {displayperson}
                    </div>
                </li>
            );
        }
    });
    var TodoList = React.createClass({
        render: function () {
            var items = [];
            for (var i = 0; i < this.props.listetodos.length; i++) {
                items.push(<TodoListItem todo={this.props.listetodos[i]}/>);
            }
            return (
                <ul className="list-group">
                    {items}
                </ul>
            );
        }
    });
    var TodoForm = React.createClass({
        getInitialState: function () {
            return {'val': "", person: ""}
        },
        handleChange: function (event) {
            this.setState({'val': event.target.value});
        },
        handleChangeName: function (event) {
            this.setState({'person': event.target.value});
        },
        handleSubmit: function (event) {
            event.preventDefault();
            this.props.update(this.state.val, this.state.person);
            this.setState({val: '', person: ''});
        },
        render: function () {
            return (
                <form className="form-horizontal" onSubmit={this.handleSubmit}>
                    <div className="form-group">
                        <label className="control-label col-md-4" for="val">Intitulé</label>
                        <div className="col-md-5">
                            <input type="text" id="val" className="form-control" onChange={this.handleChange} value={this.state.val}/>
                        </div>
                    </div>
                    <div className="form-group">
                        <label className="control-label col-md-4" for="person">Attribué à</label>
                        <div className="col-md-5">
                            <input type="text" id="person" className="form-control" onChange={this.handleChangeName} value={this.state.person}/>
                        </div>
                    </div>
                    <div className="form-group">
                        <div className="col-md-offset-4 col-md-4">
                            <button type="submit" value="Submit" className="btn btn-default">Ajouter</button>
                        </div>
                    </div>
                </form>
            );
        }
    });
    var TodoFiltre = React.createClass({
        getInitialState: function () {
            return {'faits': true, 'nonfaits': true, 'personne': ''}
        },
        handleClick: function (event) {
            this.setState({'faits': !this.state.faits});
        },
        handleClickNot: function (event) {
            this.setState({'nonfaits': !this.state.nonfaits});
        },
        handleChangePersonne: function (event) {
            this.setState({personne: event.target.value});
        },
        filter: function () {
            let res = [];
            for (let i in this.props.listetodos) {
                if (this.state.personne == '' || this.state.personne == this.props.listetodos[i].get("person")) {
                    if (this.state.faits) {
                        if (this.props.listetodos[i].get("done"))
                            res.push(this.props.listetodos[i]);
                    }
                    if (this.state.nonfaits) {
                        if (!this.props.listetodos[i].get("done"))
                            res.push(this.props.listetodos[i]);
                    }
                }
            }
            return res;
        },
        render: function () {
            let res = this.filter();
            return (
                <div>
                    <form className="form-horizontal">
                        <div className="form-group">
                            <label className="control-label col-md-4"  for="email">Filtrer par personne</label>
                            <div className="col-md-5">
                                <input type="text"  className="form-control" onChange={this.handleChangePersonne} value={this.state.personne}/>
                            </div>
                        </div>
                        <div className="form-group">
                            <div className="checkbox">
                                <label className="control-label col-md-4" >
                                <input type="checkbox"  defaultChecked={this.state.faits} onClick={this.handleClick}
                                              value={this.state.faits} />Tâches accomplies</label>
                            </div>
                        </div>
                        <div className="form-group">
                            <div className="checkbox">
                                <label className="control-label col-md-4" >
                                <input type="checkbox" defaultChecked={this.state.nonfaits}
                                              onClick={this.handleClickNot} value={this.state.nonfaits} />Tâches à faire</label>
                            </div>
                        </div>
                    </form>
                    <div className="col-md-offset-4  col-md-4">
                        <TodoList listetodos={res}/>
                    </div>
                </div>
            );
        }
    });
    var TodoApp = React.createClass({
        mixins: [backboneMixin],
        updateItems: function (newItem, person) {
            var newItemID = todo_items.length + 1;
            var it = {id: newItemID, txt: newItem, person: person, done : false}
            socket.send(JSON.stringify(it));
        },
        render: function () {
            return (
                <div>
                    <div className="row">
                        <TodoBanner listetodos={this.state.collection}/>
                    </div>
                    <div className="row">
                        <TodoFiltre listetodos={this.props.collection.models}/>
                    </div>
                    <div className="row">
                        <TodoForm update={this.updateItems}/>
                    </div>
                </div>
            );
        }
    });

    ReactDOM.render(<TodoApp collection={todo_items}/>, document.getElementById('todo'));
};